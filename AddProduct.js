import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Container,  Row3, MenuText, SubMenu, Topic, Row2,Row,
    Content, NormalColumn, Header, ColumnEditPage,UserInput, UserInputBox,Column
} from './styledComponent'
class AddProduct extends Component {
    state = {
        image: 'ss',
        name: 'ss'
    }
    UNSAFE_componentWillMount() {
        
    } 
    back=()=>{
        this.props.history.goBack()
    }
    onSave=()=>{
        this.props.history.push('/Main')
    }
    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: '#a300cc' }}>
                    <SubMenu style={{ backgroundColor: '#a300cc',marginRight: 5}} onPress={this.back}>
                    <Text style={{color: 'white'}}>Back</Text></SubMenu>
                    <MenuText>Add Product</MenuText>
                </Header>
                <Row2 style={{ backgroundColor: '#e6e6e6' }}>
                    <Column style={{marginTop: '10%'}}>
                    <UserInputBox>
                            <UserInput clear placeholder="Image Url"
                           
                            onChangeText={(image) => this.setState({ image })}></UserInput>
                            <UserInput clear placeholder="name"
                           
                            onChangeText={(name) => this.setState({ name })} ></UserInput>
                        </UserInputBox>
                    </Column>
                    
                </Row2>
                <Row3 style={{
                    backgroundColor: '#e6e6e6',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <SubMenu type="primary" style={{width: '80%', backgroundColor:'#0000b3'}}
                    onPress={this.onSave}>
                       Save</SubMenu>
                </Row3>
            </Container >
        )
    }
}

export default AddProduct