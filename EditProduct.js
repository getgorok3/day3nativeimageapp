import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Container,  Row3, MenuText, SubMenu, Topic, Row2,Row,
    Content, NormalColumn, Header, ColumnEditPage,UserInput, UserInputBox,Column
} from './styledComponent'
class EditProduct extends Component {
    state = {
        image: '',
        name: ''
    }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state || this.props.location.state.image
            || this.props.location.state.name  ) {
         
            this.setState({ image: this.props.location.state.image })
            this.setState({ name: this.props.location.state.name })

        } else {
            console.log('Fail Profile', this.props)
          
        } 
    } 
    back=()=>{
        this.props.history.goBack()
    }
    onSave=()=>{
        this.props.history.push('/Product', { image: this.state.image, name: this.state.name})
    }
    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: '#a300cc' }}>
                    <SubMenu style={{ backgroundColor: '#a300cc',marginRight: 5}} onPress={this.back}>
                    <Text style={{color: 'white'}}>Back</Text></SubMenu>
                    <MenuText>Edit Product</MenuText>
                </Header>
                <Row2 style={{ backgroundColor: '#e6e6e6' }}>
                    <Column style={{marginTop: '10%'}}>
                    <UserInputBox>
                            <UserInput clear placeholder="Image Url"
                           
                            onChangeText={(image) => this.setState({ image })}></UserInput>
                            <UserInput clear placeholder="name"
                           
                            onChangeText={(name) => this.setState({ name })} ></UserInput>
                        </UserInputBox>
                    </Column>
                    
                </Row2>
                <Row3 style={{
                    backgroundColor: '#e6e6e6',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <SubMenu type="primary" style={{width: '80%', backgroundColor:'#0000b3'}}
                    onPress={this.onSave}>
                       Save</SubMenu>
                </Row3>
            </Container >
        )
    }
}

export default EditProduct