import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Container,  Row3, MenuText, SubMenu, Topic, Row2,Row,
    Content, NormalColumn, Header, ColumnEditPage,UserInput, UserInputBox,Column
} from './styledComponent'
class EditProfile extends Component {
    state = {
        user: '',
        firstname: ' - ',
        lastname: ' - '
    }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state || this.props.location.state.user
            || this.props.location.state.firstname  || this.props.location.state.lastname) {
            console.log('Profile', this.props.location.state.user)
            this.setState({ user: this.props.location.state.user })
            this.setState({ firstname: this.props.location.state.firstname })
            this.setState({ lastname: this.props.location.state.lastname })

        } else {
            console.log('Fail Editpage', this.props)
           
        }

    } 
    backToMain=()=>{
        this.props.history.goBack()
    }
    onSave=()=>{
        this.props.history.push('/Profile', { firstname: this.state.firstname, lastname: this.state.lastname,
        user: this.state.user })
    }
    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: '#a300cc' }}>
                    <SubMenu style={{ backgroundColor: '#a300cc',marginRight: 5}} onPress={this.backToMain}>
                    <Text style={{color: 'white'}}>Back</Text></SubMenu>
                    <MenuText>Edit Profile</MenuText>
                </Header>
                <Row2 style={{ backgroundColor: '#e6e6e6' }}>
                    <Column style={{marginTop: '10%'}}>
                    <UserInputBox>
                            <UserInput clear placeholder="First name"
                           
                            onChangeText={(firstname) => this.setState({ firstname })}></UserInput>
                            <UserInput clear placeholder="Last name"
                           
                            onChangeText={(lastname) => this.setState({ lastname })} ></UserInput>
                        </UserInputBox>
                    </Column>
                    
                </Row2>
                <Row3 style={{
                    backgroundColor: '#e6e6e6',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <SubMenu type="primary" style={{width: '80%', backgroundColor:'#0000b3'}}
                    onPress={this.onSave}>
                       Save</SubMenu>
                </Row3>
            </Container >
        )
    }
}

export default EditProfile