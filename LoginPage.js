import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    CustomeButton, Container, Row, Row2,
    Column, UserInput, UserInputBox, LoginImageFrame, LogInImage
} from './styledComponent'

class Login extends Component {
    state = {
        username: '',
        password: '',
    }
    UNSAFE_componentWillMount() {    
        console.log('Login Page ',this.props)   
    }
    checkCorrectPass = () => {
        let user = this.state.username.toLowerCase();
        let pass = this.state.password.toLowerCase()
        if (user === 'aaaa' && pass === 'aaaa') {
            console.log(user)
            this.props.history.push('/Main', { user: user })
        } else {
            this.props.history.push('/')
        }
    }
    render() {
        return (
            <Container>
                <Row>
                    <LoginImageFrame>
                        <LogInImage resizeMode="stretch" source={{
                            uri: 'https://cdn.pixabay.com/photo/2019/01/02/21/04/unicorn-3909694_960_720.jpg'
                        }} />
                    </LoginImageFrame>
                </Row>
                <Row2>
                    <Column>
                        <UserInputBox>
                            <UserInput clear placeholder="Username"
                            value={this.state.username} 
                            onChangeText={(username) => this.setState({ username })}></UserInput>
                            <UserInput clear placeholder="Password"
                            value={this.state.password}
                            onChangeText={(password) => this.setState({ password })} ></UserInput>
                        </UserInputBox>
                        <CustomeButton type="primary" onPress={this.checkCorrectPass}>Login</CustomeButton>
                    </Column>
                </Row2>
            </Container>
        )
    }
}

export default Login