import React, { Component } from 'react';
import{ TouchableOpacity} from 'react-native';
import {
     Container, Row, Row2, MenuText, SubMenu,
     NewImage, Footer, ImageFrame, Header, NewScrollView
} from './styledComponent'
class Main extends Component {
    state = {
        user: '',
        image: '',
        name: ''
    }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state && this.props.location.state.user
           ) {
        
            this.setState({ user: this.props.location.state.user })
            this.setState({ firstname: this.props.location.state.firstname })
            this.setState({ lastname: this.props.location.state.lastname })

        } else if(this.props.location && this.props.location.state
          ){
               
            console.log('back from product Profile', this.props)
        }else {
            console.log('Fail Profile', this.props)
            this.setState({ user: 'user' })
        }

    } 

    goToProfilePage=()=>{
        this.props.history.push('/Profile',{firstname: this.state.firstname,
            lastname: this.state.lastname, user: this.state.user})
    }
    goToAddProductPage=()=>{
        this.props.history.push('/AddProduct')
    }

    onPressImage=(name,url)=>{
        this.setState({image: url})
        this.setState({name: name})
        this.props.history.push('/Product',{image: this.state.image,
            name: this.state.name})
    }
    logOut=()=>{
        this.props.history.push('/')
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#0077b3'}}>
                    <MenuText>List</MenuText>
                </Header>
                <NewScrollView>

                    <Row2>
                        <ImageFrame 
                       >
                       <TouchableOpacity onPress={()=>{this.onPressImage('cat','https://meow-meow.jp/wp-content/uploads/2017/04/猫.しつけ.オススメ.水スプレー.霧吹き.ミャウミャウ.7.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://meow-meow.jp/wp-content/uploads/2017/04/猫.しつけ.オススメ.水スプレー.霧吹き.ミャウミャウ.7.jpg'
                            }}  />
                       </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame 
                        >
                        <TouchableOpacity onPress={()=>{this.onPressImage('turtle','http://www.thinkstockphotos.com/ts-resources/images/home/TS_AnonHP_462882495_01.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'http://www.thinkstockphotos.com/ts-resources/images/home/TS_AnonHP_462882495_01.jpg'
                            }}  />
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>

                    <Row2>
                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('sea ​​lion','http://p3.ifengimg.com/haina/2016_48/46f33309a83ab55_w1000_h667.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'http://p3.ifengimg.com/haina/2016_48/46f33309a83ab55_w1000_h667.jpg'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('panda','https://animals.sandiegozoo.org/sites/default/files/2016-08/category-thumbnail-mammals_0.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://animals.sandiegozoo.org/sites/default/files/2016-08/category-thumbnail-mammals_0.jpg'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>

                    <Row2>
                        <ImageFrame
                        >
                        <TouchableOpacity onPress={()=>{this.onPressImage('monkey','https://ctnbq.org/wp-content/uploads/2014/08/animals-smile_3379238k.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://ctnbq.org/wp-content/uploads/2014/08/animals-smile_3379238k.jpg'
                            }}  />
                            </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('Koala','https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Mammals/H-P/koala-closeup-tree.adapt.945.1.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Mammals/H-P/koala-closeup-tree.adapt.945.1.jpg'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>

                    <Row2>
                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('dragon','https://www.buildabear.co.uk/dw/image/v2/BBNG_PRD/on/demandware.static/-/Sites-buildabear-master/default/dw8843e75f/27017x.jpg?sw=600&sh=600&sm=fit')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://www.buildabear.co.uk/dw/image/v2/BBNG_PRD/on/demandware.static/-/Sites-buildabear-master/default/dw8843e75f/27017x.jpg?sw=600&sh=600&sm=fit'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('polar bear','http://p0.ifengimg.com/fck/2018_32/501fd93670d6684_w727_h409.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'http://p0.ifengimg.com/fck/2018_32/501fd93670d6684_w727_h409.jpg'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>

                    <Row2>
                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('owl','https://static.s-sfr.fr/media/5927758528_a2060423e7_b.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://static.s-sfr.fr/media/5927758528_a2060423e7_b.jpg'
                            }}/>
                            </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame
                         >
                         <TouchableOpacity  onPress={()=>{this.onPressImage('tiger','https://bestscreenwallpaper.pro/wp-content/uploads/2013/08/Tiger-AnimalFree-Wallpapers.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://bestscreenwallpaper.pro/wp-content/uploads/2013/08/Tiger-AnimalFree-Wallpapers.jpg'
                            }}/>
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>

                    <Row2>
                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('Yujiro','https://steamuserimages-a.akamaihd.net/ugc/963105290819212874/77F34946DFBF00E627EA33E673AC815D73D7CE9B/')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://steamuserimages-a.akamaihd.net/ugc/963105290819212874/77F34946DFBF00E627EA33E673AC815D73D7CE9B/'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>

                        <ImageFrame
                         >
                         <TouchableOpacity onPress={()=>{this.onPressImage('Dio','https://www.meme-arsenal.com/memes/5ad0647292f1d4a827e1250542862dee.jpg')}}>
                            <NewImage resizeMode="stretch" source={{
                                uri: 'https://www.meme-arsenal.com/memes/5ad0647292f1d4a827e1250542862dee.jpg'
                            }} />
                            </TouchableOpacity>
                        </ImageFrame>
                    </Row2>
                </NewScrollView>
                <Footer style={{backgroundColor: '#0000cc'}}> 
                    <SubMenu style={{backgroundColor: '#00cc66'}} onPress={this.logOut}>L</SubMenu> 
                    <TouchableOpacity onPress={this.goToAddProductPage} style={{width: '78%'}}>
                    <MenuText>Add</MenuText>
                    </TouchableOpacity>
                    <SubMenu style={{backgroundColor: '#cc66ff'}}
                    onPress={this.goToProfilePage}>P</SubMenu>
                </Footer>
            </Container>
        )
    }
}
export default Main