import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Container,  Row3, MenuText, SubMenu, Topic,
    Content, NormalColumn, Header, ColumnEditPage
} from './styledComponent'
class Product extends Component {
    state = {
      image: '',
      name: ''
       
    }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state || this.props.location.state.image
            || this.props.location.state.name  ) {
          
            this.setState({ image: this.props.location.state.image })
            this.setState({ name: this.props.location.state.name })

        } else {
            console.log('Fail Profile', this.props)
            
        }
    } 
    backToMain=()=>{
        this.props.history.push('/Main')
    }
    goToEditPage=()=>{
        this.props.history.push('/EditProduct',{image: this.state.image,
            name: this.state.name})

    }
    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: '#a300cc' }}>
                    <SubMenu style={{ backgroundColor: '#a300cc',marginRight: 5}} onPress={this.backToMain}>
                    <Text style={{color: 'white'}}>Back</Text></SubMenu>
                    <MenuText>Product</MenuText>
                </Header>
                <Row3 style={{ backgroundColor: '#e6e6e6' }}>
                    <ColumnEditPage >
                        <Row3>
                            <NormalColumn>
                                <Topic>Image</Topic>
                                <Content>{this.state.image}</Content>
                            </NormalColumn>
                        </Row3>

                        <Row3>
                            <NormalColumn>
                                <Topic>Name</Topic>
                                <Content>{this.state.name}</Content>
                            </NormalColumn>
                        </Row3>
                    </ColumnEditPage>
                </Row3>
                <Row3 style={{
                    backgroundColor: '#e6e6e6',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <SubMenu type="primary" style={{width: '80%',backgroundColor:'#0000b3'}} onPress={this.goToEditPage}>
                       Edit</SubMenu>
                </Row3>
            </Container >
        )
    }
}

export default Product