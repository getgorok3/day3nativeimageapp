import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    Container,  Row3, MenuText, SubMenu, Topic,
    Content, NormalColumn, Header, ColumnEditPage
} from './styledComponent'
class Profile extends Component {
    state = {
        user: '',
        firstname: ' - ',
        lastname: ' - '
    }
    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state || this.props.location.state.user
            || this.props.location.state.firstname  || this.props.location.state.lastname) {
            console.log('Profile', this.props.location.state.user)
            this.setState({ user: this.props.location.state.user })
            this.setState({ firstname: this.props.location.state.firstname })
            this.setState({ lastname: this.props.location.state.lastname })

        } else {
            console.log('Fail Profile', this.props)
            this.setState({ user: 'user' })
        }

    } 
    backToMain=()=>{
        this.props.history.push('/Main',{firstname: this.state.firstname,
            lastname: this.state.lastname, user: this.state.user})
    }
    goToEditPage=()=>{
        this.props.history.push('/EditProfile',{firstname: this.state.firstname,
        lastname: this.state.lastname, user: this.state.user})
    }
    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: '#a300cc' }}>
                    <SubMenu style={{ backgroundColor: '#a300cc',marginRight: 5}} onPress={this.backToMain}>
                    <Text style={{color: 'white'}}>Back</Text></SubMenu>
                    <MenuText>My Profile</MenuText>
                </Header>
                <Row3 style={{ backgroundColor: '#e6e6e6' }}>
                    <ColumnEditPage >
                        <Row3>
                            <NormalColumn>
                                <Topic>Username</Topic>
                                <Content>{this.state.user}</Content>
                            </NormalColumn>
                        </Row3>

                        <Row3>
                            <NormalColumn>
                                <Topic>First name</Topic>
                                <Content>{this.state.firstname}</Content>
                            </NormalColumn>
                        </Row3>

                        <Row3>
                            <NormalColumn>
                                <Topic>Last name</Topic>
                                <Content>{this.state.lastname}</Content>
                            </NormalColumn>
                        </Row3>
                    </ColumnEditPage>
                </Row3>
                <Row3 style={{
                    backgroundColor: '#e6e6e6',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <SubMenu type="primary" style={{width: '80%',backgroundColor:'#0000b3'}} onPress={this.goToEditPage}>
                       Edit</SubMenu>
                </Row3>
            </Container >
        )
    }
}

export default Profile