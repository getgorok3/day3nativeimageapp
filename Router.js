import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './LoginPage'
import Main from './MainPage'
import Profile from './ProfilePage'
import EditProfile from './EditProfilePage'
import Product from './ProductPage'
import EditProduct from './EditProduct'
import AddProduct from './AddProduct'
class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/" component={Login} />
                    {/* <Route exact path="/Login" component={Login} />  */}
                    <Route exact path="/Main" component={Main} />
                    <Route exact path="/Profile" component={Profile} />
                    <Route exact path="/EditProfile" component={EditProfile} />
                    <Route exact path="/Product" component={Product} />
                    <Route exact path="/EditProduct" component={EditProduct} />
                    <Route exact path="/AddProduct" component={AddProduct} />
                    {/* <Redirect to="/screen1" /> */}
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router